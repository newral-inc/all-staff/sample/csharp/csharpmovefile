﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CSharpMoveFile
{
    class Program
    {
        static void Main(string[] args)
        {
            // コピー元、コピー先のディレクトリを一旦削除します。
            var currentDir = Directory.GetCurrentDirectory();
            var srcDir = Path.Combine(currentDir, "SrcDir");
            var dstDir = Path.Combine(currentDir, "DstDir");
            Directory.Delete(srcDir, true);
            Directory.Delete(dstDir, true);
            Console.WriteLine($"SrcDir exists={Directory.Exists(srcDir)}");
            Console.WriteLine($"DstDir exists={Directory.Exists(dstDir)}");

            // コピー元、コピー先のディレクトリを作成します。
            Directory.CreateDirectory(srcDir);
            Directory.CreateDirectory(dstDir);
            Console.WriteLine($"SrcDir exists={Directory.Exists(srcDir)}");
            Console.WriteLine($"DstDir exists={Directory.Exists(dstDir)}");

            // コピー元ファイルを作成します。
            var srcFilePath = Path.Combine(srcDir, "src.txt");
            var dstFilePath = Path.Combine(dstDir, "dst.txt");
            var stream = File.Create(srcFilePath);
            stream.Close();
            Console.WriteLine($"SrcFile exists={File.Exists(srcFilePath)}");
            Console.WriteLine($"DstFile exists={File.Exists(dstFilePath)}");

            // コピー元ファイルを移動します。
            File.Move(srcFilePath, dstFilePath);
            Console.WriteLine($"SrcFile exists={File.Exists(srcFilePath)}");
            Console.WriteLine($"DstFile exists={File.Exists(dstFilePath)}");
        }
    }
}
